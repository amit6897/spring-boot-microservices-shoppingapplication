package com.programming.productservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document(value = "product")    // to define this product as a mongodb document we use this @Document annotation
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Product {
    @Id     // this is a unique identifier for our product
    private String id;
    private String name;
    private String description;
    private BigDecimal price;
}

/*
Important Points to Remember :
-> To store this product inside the database we have to create a spring data repository
 */