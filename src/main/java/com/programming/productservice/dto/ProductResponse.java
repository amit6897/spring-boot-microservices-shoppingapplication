package com.programming.productservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {
    private String id;
    private String name;
    private String description;
    private BigDecimal price;
}

/*
Q. Why we have created a separate class called as ProductResponse, why can't we directly use the Product class
   from the model package ?
--> It's a good practice to separate model entities and the dtos, so ideally we should not expose our model entities to
    the outside world because if in the future in this Product class if i have added some other two fields which are
    necessary for the business model, we should not expose these two fields which are not necessary to the outside world.
*/
